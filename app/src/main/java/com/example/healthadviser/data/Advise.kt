package com.example.healthadviser.data

import androidx.annotation.Keep
import java.io.Serializable

@Keep
data class Advise(val header : String, val text : String) :Serializable
