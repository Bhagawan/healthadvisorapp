package com.example.healthadviser.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class RoutineItem(val time: Int, @SerializedName("for") val routineList : List<String>)
