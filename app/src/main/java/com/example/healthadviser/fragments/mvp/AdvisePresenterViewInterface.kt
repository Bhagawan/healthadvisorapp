package com.example.healthadviser.fragments.mvp

import com.example.healthadviser.data.Advise
import moxy.MvpView
import moxy.viewstate.strategy.alias.SingleState

interface AdvisePresenterViewInterface : MvpView {

    @SingleState
    fun fillAdvises(advises :List<Advise>)

    @SingleState
    fun showServerError()
}