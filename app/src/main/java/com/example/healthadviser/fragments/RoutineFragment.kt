package com.example.healthadviser.fragments

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.healthadviser.R
import com.example.healthadviser.data.RoutineItem
import com.example.healthadviser.fragments.adapter.RoutineAdapter
import com.example.healthadviser.fragments.mvp.RoutinePresenter
import com.example.healthadviser.fragments.mvp.RoutinePresenterViewInterface
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class RoutineFragment : MvpAppCompatFragment(), RoutinePresenterViewInterface {
    private lateinit var mView :View
    private lateinit var target :Target

    @InjectPresenter
    lateinit var mPresenter: RoutinePresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.fragment_routine, container, false)
        context?.let { mPresenter.start(it) }
        setBackground()
        return mView
    }

    override fun fillRoutine(routine: List<RoutineItem>) {
        val recycler : RecyclerView = mView.findViewById(R.id.recycler_routine)
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = RoutineAdapter(routine)

    }

    override fun showError(error: String) = Toast.makeText(context, error, Toast.LENGTH_SHORT).show()

    private fun setBackground() {
        val backLayout = mView.findViewById<ConstraintLayout>(R.id.layout_routine_main)
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                backLayout.background = BitmapDrawable(resources, bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/HealthAdviserApp/back.png").into(target)
    }
}