package com.example.healthadviser.fragments.mvp

import android.content.Context
import com.example.healthadviser.R
import com.example.healthadviser.data.RoutineItem
import com.example.healthadviser.util.ServerClient
import com.example.healthadviser.util.SharedPref
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@InjectViewState
class RoutinePresenter :MvpPresenter<RoutinePresenterViewInterface>() {

    fun start(context: Context) {
        SharedPref.update(context)

        val call = ServerClient.create().getRoutine()
        call.enqueue(object : Callback<List<RoutineItem>> {
            override fun onResponse(
                call: Call<List<RoutineItem>>,
                response: Response<List<RoutineItem>>
            ) {
                if(response.isSuccessful) {
                    response.body()?.let { viewState.fillRoutine(it) }
                } else viewState.showError(context.getString(R.string.msg_error_server))
            }

            override fun onFailure(call: Call<List<RoutineItem>>, t: Throwable) {
                viewState.showError(context.getString(R.string.msg_error_server))
            }
        })
    }
}