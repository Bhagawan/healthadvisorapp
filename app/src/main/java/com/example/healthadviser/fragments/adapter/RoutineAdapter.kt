package com.example.healthadviser.fragments.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.healthadviser.R
import com.example.healthadviser.data.RoutineItem
import com.example.healthadviser.util.SharedPref

class RoutineAdapter(private val items : List<RoutineItem>) : RecyclerView.Adapter<RoutineAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_routine, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val headerString = items[position].time.toString() + " " + holder.itemView.context.getString(R.string.msg_routine_end)
        holder.header.text = headerString
        setCheckMark(holder.itemView.context, holder.img, position)

        holder.itemView.setOnClickListener { onClick(holder.itemView.context, holder.img, holder.adapterPosition) }
        holder.recycler.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                if(p1?.action == MotionEvent.ACTION_UP) {
                    onClick(holder.itemView.context, holder.img, holder.adapterPosition)
                    return true
                }
                return false
            }
        })

        val adapter = SimpleStringAdapter(items[position].routineList)
        holder.recycler.layoutManager = LinearLayoutManager(holder.itemView.context)
        holder.recycler.adapter = adapter
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val header: TextView = itemView.findViewById(R.id.text_routine_item)
        val img : ImageView = itemView.findViewById(R.id.imageView_routine_item_check)
        val recycler : RecyclerView = itemView.findViewById(R.id.recycler_routine_item)

        init {
            itemView.setOnClickListener {
                SharedPref.setStage(it.context, adapterPosition, !SharedPref.getStage(it.context, adapterPosition))
                setCheckMark(it.context, img, adapterPosition)
                notifyItemChanged(adapterPosition)
            }
        }
    }


    private fun setCheckMark(context : Context, img :ImageView, pos : Int) {
        if(SharedPref.getStage(context, pos))  {
            img.setImageDrawable(ResourcesCompat.getDrawable(context.resources
                ,R.drawable.ic_outline_check_box_24, null))
        } else img.setImageDrawable(ResourcesCompat.getDrawable(context.resources
            ,R.drawable.ic_outline_check_box_outline_blank_24, null))
    }

    fun onClick(context : Context, img :ImageView, position : Int) {
        SharedPref.setStage(context, position, !SharedPref.getStage(context, position))
        setCheckMark(context,img, position)
        notifyItemChanged(position)
    }
}