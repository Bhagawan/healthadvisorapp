package com.example.healthadviser.fragments.mvp

import com.example.healthadviser.data.RoutineItem
import moxy.MvpView
import moxy.viewstate.strategy.alias.SingleState

interface RoutinePresenterViewInterface : MvpView {

    @SingleState
    fun fillRoutine(routine :List<RoutineItem>)

    @SingleState
    fun showError(error: String)
}