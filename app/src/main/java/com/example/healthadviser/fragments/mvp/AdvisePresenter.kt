package com.example.healthadviser.fragments.mvp

import com.example.healthadviser.data.Advise
import com.example.healthadviser.util.ServerClient
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@InjectViewState
class AdvisePresenter : MvpPresenter<AdvisePresenterViewInterface>() {

    override fun onFirstViewAttach() {
        val call = ServerClient.create().getAdvices()
        call.enqueue(object : Callback<List<Advise>>{
            override fun onResponse(call: Call<List<Advise>>, response: Response<List<Advise>>) {
                if(response.isSuccessful) response.body()?.let {viewState.fillAdvises(it)}
                else viewState.showServerError()
            }
            override fun onFailure(call: Call<List<Advise>>, t: Throwable) {
                viewState.showServerError()
            }
        })
    }
}