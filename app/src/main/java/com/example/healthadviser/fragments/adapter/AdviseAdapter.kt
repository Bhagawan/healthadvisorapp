package com.example.healthadviser.fragments.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.healthadviser.R
import com.example.healthadviser.data.Advise

class AdviseAdapter (private val items : List<Advise>) : RecyclerView.Adapter<AdviseAdapter.ViewHolder>() {
    lateinit var callback: Callback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_advise, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.text.text = items[position].header.take(50)
        holder.itemView.setOnClickListener { callback.onClick(position) }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val text: TextView = itemView.findViewById(R.id.text_advise_item)
    }

    fun setOnTouchCallback(callback :Callback) {
        this.callback = callback
    }

    interface Callback {
        fun onClick(position :Int)
    }
}