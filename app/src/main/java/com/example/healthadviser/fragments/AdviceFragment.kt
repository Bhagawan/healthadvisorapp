package com.example.healthadviser.fragments

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.healthadviser.AdviseActivity
import com.example.healthadviser.R
import com.example.healthadviser.data.Advise
import com.example.healthadviser.fragments.adapter.AdviseAdapter
import com.example.healthadviser.fragments.mvp.AdvisePresenter
import com.example.healthadviser.fragments.mvp.AdvisePresenterViewInterface
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class AdviceFragment : MvpAppCompatFragment(), AdvisePresenterViewInterface {
    private lateinit var mView : View
    private lateinit var target : Target

    @InjectPresenter
    lateinit var mPresenter : AdvisePresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.fragment_advice, container, false)
        setBackground()
        return mView
    }

    override fun fillAdvises(advises: List<Advise>) {
        val recycler : RecyclerView = mView.findViewById(R.id.recycler_advise)
        recycler.layoutManager = LinearLayoutManager(context)
        val adapter = AdviseAdapter(advises)
        adapter.setOnTouchCallback(object : AdviseAdapter.Callback {
            override fun onClick(position: Int) {
                showAdvise(advises[position])
            }
        })
        recycler.adapter = adapter
    }

    override fun showServerError() = Toast.makeText(context, getString(R.string.msg_error_server), Toast.LENGTH_SHORT).show()

    fun showAdvise(advise : Advise) {
        val intent = Intent(context, AdviseActivity::class.java)
        intent.putExtra("advise", advise)
        startActivity(intent)
    }

    private fun setBackground() {
        val backLayout = mView.findViewById<LinearLayout>(R.id.layout_advise_main)
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                backLayout.background = BitmapDrawable(resources, bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/HealthAdviserApp/back.png").into(target)
    }
}