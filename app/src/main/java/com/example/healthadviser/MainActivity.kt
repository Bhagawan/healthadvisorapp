package com.example.healthadviser

import android.animation.Animator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.healthadviser.fragments.AdviceFragment
import com.example.healthadviser.fragments.RoutineFragment

class MainActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager2

    override fun onBackPressed() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fullscreen()
        val backButton = findViewById<ImageView>(R.id.imageButton_back)
        val forwardButton = findViewById<ImageView>(R.id.imageButton_forward)

        viewPager = findViewById(R.id.pager)
        val pagerAdapter = ScreenSlidePagerAdapter(this)
        viewPager.adapter = pagerAdapter

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if(position == 0) {
                    backButton.visibility = View.GONE
                    forwardButton.visibility = View.VISIBLE
                } else if(position == 1) {
                    backButton.visibility = View.VISIBLE
                    forwardButton.visibility = View.GONE
                }
            }
        })

        backButton.setOnClickListener { dragToPage(viewPager.currentItem - 1) }
        forwardButton.setOnClickListener { dragToPage(viewPager.currentItem + 1) }
    }

    private inner class ScreenSlidePagerAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
        override fun getItemCount(): Int = 2

        override fun createFragment(position: Int): Fragment = when(position) {
            0 -> RoutineFragment()
            1 -> AdviceFragment()
            else -> { RoutineFragment() }
        }
    }

    private fun fullscreen() {
        WindowInsetsControllerCompat(window, window.decorView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }

    private fun dragToPage(page : Int) {
        val distance = (viewPager.currentItem - page) * resources.displayMetrics.widthPixels.toFloat()
        val animator = ValueAnimator.ofFloat(0.0f, distance)
        animator.duration = 300
        animator.addUpdateListener { p0 -> viewPager.fakeDragBy(p0?.animatedValue as Float) }
        animator.addListener(object :Animator.AnimatorListener{
            override fun onAnimationStart(p0: Animator?) {
                viewPager.beginFakeDrag()
            }

            override fun onAnimationEnd(p0: Animator?) {
                viewPager.endFakeDrag()
            }
            override fun onAnimationCancel(p0: Animator?) {}
            override fun onAnimationRepeat(p0: Animator?) {}
        })
        animator.start()
    }

}