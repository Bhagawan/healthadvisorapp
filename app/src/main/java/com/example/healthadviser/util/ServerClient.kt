package com.example.healthadviser.util

import com.example.healthadviser.data.Advise
import com.example.healthadviser.data.RoutineItem
import com.example.healthadviser.data.SplashResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ServerClient {

    @GET("HealthAdviserApp/EverydayRoutine.json")
    fun getRoutine() : Call<List<RoutineItem>>

    @GET("HealthAdviserApp/HealthAdvices.json")
    fun getAdvices() : Call<List<Advise>>

    @FormUrlEncoded
    @POST("HealthAdviserApp/splash.php")
    fun getSplash(@Field("locale") locale: String): Call<SplashResponse>

    companion object {
        fun create() : ServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ServerClient::class.java)
        }
    }

}