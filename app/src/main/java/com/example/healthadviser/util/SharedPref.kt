package com.example.healthadviser.util

import android.content.Context
import android.content.SharedPreferences
import java.text.SimpleDateFormat
import java.util.*

class SharedPref {
    companion object {
        fun update(context: Context) {
            val sharedPreferences: SharedPreferences =
                context.getSharedPreferences("SavedRoutine", Context.MODE_PRIVATE)
            val myEdit = sharedPreferences.edit()

            val dateFormat = SimpleDateFormat("yy,MM,dd", Locale.getDefault())
            val currDate = dateFormat.format(Date())

            val savedDate = sharedPreferences.getString("date", "")
            if(savedDate.equals("") || !savedDate.equals(currDate)) {
                myEdit.clear()
                myEdit.putString("date", currDate)
            }
            myEdit.apply()
        }

        fun setStage(context: Context, stage : Int, state: Boolean) {
            val sharedPreferences: SharedPreferences =
                context.getSharedPreferences("SavedRoutine", Context.MODE_PRIVATE)
            val myEdit = sharedPreferences.edit()
            myEdit.putBoolean(stage.toString(), state)
            myEdit.apply()
        }

        fun getStage(context: Context, stage : Int): Boolean {
            val sharedPreferences : SharedPreferences = context.getSharedPreferences("SavedRoutine", Context.MODE_PRIVATE)
            return sharedPreferences.getBoolean(stage.toString(), false)
        }

        fun reset(context: Context) = context.getSharedPreferences("SavedRoutine", Context.MODE_PRIVATE).edit().clear().apply()
    }
}