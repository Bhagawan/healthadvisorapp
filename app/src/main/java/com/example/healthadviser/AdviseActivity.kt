package com.example.healthadviser

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.example.healthadviser.data.Advise
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class AdviseActivity : AppCompatActivity() {

    lateinit var target: Target

    override fun onBackPressed() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_advise)
        fullscreen()
        setBackground()

        intent?.let {
            val b = intent.getSerializableExtra("advise") as Advise
            fillAdviseRecycler(b)
        }

        val backButton = findViewById<ImageButton>(R.id.imageButton_advise_back)
        backButton.setOnClickListener { finish() }
    }

    private fun fillAdviseRecycler(advise: Advise) {
        val headerView = findViewById<TextView>(R.id.textView_advise_header)
        val adviseView = findViewById<TextView>(R.id.textView_advise_text)
        headerView.text = advise.header
        adviseView.text = advise.text
    }

    private fun fullscreen() {
        WindowInsetsControllerCompat(window, window.decorView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }


    private fun setBackground() {
        val backLayout = findViewById<ScrollView>(R.id.scrollView_advise)
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                backLayout.background = BitmapDrawable(resources, bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/HealthAdviserApp/back.png").into(target)
    }
}